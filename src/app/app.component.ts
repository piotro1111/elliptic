import {Component} from '@angular/core';
import {ec} from 'elliptic';
import CircularJSON from 'circular-json';
@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent {
  public key;
  public a;

  constructor() {
    this.key = ec('curve25519');
    this.a = this.key.genKeyPair();

    console.log(
      'ec: ', this.a.ec as JSON,
      '\nprivate: ', this.a.priv,
      '\npublic: ', this.a.pub
    );
  }

  _JSON = (object) => CircularJSON.stringify(object, undefined, 2);


}
